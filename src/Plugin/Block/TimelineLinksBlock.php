<?php

namespace Drupal\entity_timeline\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Http\RequestStack;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the timeline links block.
 *
 * @Block(
 *   id = "timeline_links_block",
 *   admin_label = @Translation("Timeline links"),
 *   category = @Translation("Lists (Views)")
 * )
 */
class TimelineLinksBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The request stack.
   *
   * @var \Drupal\Core\Http\RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected CurrentRouteMatch $currentRouteMatch;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected AccountProxyInterface $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->requestStack = $container->get('request_stack');
    $instance->currentRouteMatch = $container->get('current_route_match');
    $instance->currentUser = $container->get('current_user');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    if ($user = $this->currentRouteMatch->getParameter('user')) {
      return AccessResult::allowedIf($account->id() === $user->id());
    }
    return AccessResult::forbidden();
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $cache = new CacheableMetadata();
    $cache->addCacheContexts(['route', 'url.query_args', 'user']);
    $build = [
      '#attached' => [
        'library' => ['entity_timeline/links'],
      ],
    ];

    $request = $this->requestStack->getCurrentRequest();
    $path = $request->getPathInfo();
    $query = $request->query->all();
    unset($query['page']);
    $value = $query['type'] ?? NULL;

    // Links to filter timeline by ownership type.
    $build['links'] = [
      '#theme' => 'links__entity_timeline',
      '#links' => [
        [
          'title' => $this->t('All'),
          'url' => Url::fromUserInput($path, [
            'query' => ['type' => ''] + $query,
          ]),
          'attributes' => [
            'class' => !$value ? ['active'] : [],
          ],
        ],
        [
          'title' => $this->t('My activity'),
          'url' => Url::fromUserInput($path, [
            'query' => ['type' => 'user'] + $query,
          ]),
          'attributes' => [
            'class' => $value === 'user' ? ['active'] : [],
          ],
        ],
        [
          'title' => $this->t('Followed activity'),
          'url' => Url::fromUserInput($path, [
            'query' => ['type' => 'follow'] + $query,
          ]),
          'attributes' => [
            'class' => $value === 'follow' ? ['active'] : [],
          ],
        ],
      ],
      '#attributes' => [
        'class' => ['entity-timeline-links', 'inline'],
      ],
    ];

    $cache->addCacheableDependency($this->currentRouteMatch->getParameter('user'));
    $cache->applyTo($build);
    return $build;
  }

}
