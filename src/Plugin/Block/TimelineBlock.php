<?php

namespace Drupal\entity_timeline\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\entity_timeline\TimelineBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the timeline block.
 *
 * @Block(
 *   id = "timeline_block",
 *   admin_label = @Translation("Timeline"),
 *   category = @Translation("Lists (Views)")
 * )
 */
class TimelineBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected CurrentRouteMatch $currentRouteMatch;

  /**
   * The timeline builder.
   *
   * @var \Drupal\entity_timeline\TimelineBuilder
   */
  protected TimelineBuilder $timelineBuilder;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->currentRouteMatch = $container->get('current_route_match');
    $instance->timelineBuilder = $container->get('entity_timeline.builder');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    if ($account = $this->currentRouteMatch->getParameter('user')) {
      $this->timelineBuilder->setAccount($account);
    }
    return $this->timelineBuilder->build();
  }

}
