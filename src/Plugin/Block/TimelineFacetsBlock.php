<?php

namespace Drupal\entity_timeline\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\entity_timeline\Form\TimelineFacetsForm;
use Drupal\entity_timeline\TimelineBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the timeline facets block.
 *
 * @Block(
 *   id = "timeline_facets_block",
 *   admin_label = @Translation("Timeline facets"),
 *   category = @Translation("Lists (Views)")
 * )
 */
class TimelineFacetsBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected FormBuilderInterface $formBuilder;

  /**
   * The timeline builder.
   *
   * @var \Drupal\entity_timeline\TimelineBuilder
   */
  protected TimelineBuilder $timelineBuilder;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->formBuilder = $container->get('form_builder');
    $instance->timelineBuilder = $container->get('entity_timeline.builder');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIf(count($this->timelineBuilder->getItems()));
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    return $this->formBuilder->getForm(TimelineFacetsForm::class);
  }

}
