<?php

namespace Drupal\entity_timeline\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\entity_timeline\Annotation\TimelineItemType;

/**
 * Provides timeline item type plugin manager.
 */
class TimelineItemTypeManager extends DefaultPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/TimelineItemType',
      $namespaces,
      $module_handler,
      TimelineItemTypeInterface::class,
      TimelineItemType::class
    );
    $this->alterInfo('timeline_item_type_info');
    $this->setCacheBackend($cache_backend, 'timeline_item_type_plugins');
  }

}
