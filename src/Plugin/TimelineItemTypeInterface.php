<?php

namespace Drupal\entity_timeline\Plugin;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the timeline item type plugin interface.
 */
interface TimelineItemTypeInterface extends ConfigurableInterface, DependentPluginInterface, PluginFormInterface, PluginInspectionInterface {

  /**
   * Get bundles as options.
   *
   * @param \Drupal\Core\Entity\EntityInterface[] $bundles
   *   The entity type bundles.
   *
   * @return array
   *   Returns an associative array of bundle options.
   */
  public function getBundleOptions(array $bundles): array;

  /**
   * Get bundle configuration.
   *
   * @param string $bundle
   *   The bundles name.
   *
   * @return array
   *   Returns the bundle configuration.
   */
  public function getBundleConfiguration(string $bundle): array;

  /**
   * Get the entity storage table.
   *
   * @return string|null
   *   Returns the table name.
   */
  public function getTable(): ?string;

  /**
   * Get the entity storage table owner column.
   *
   * @return string|null
   *   Returns the table column name.
   */
  public function getColumn(): ?string;

  /**
   * Builds entity query.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The users account.
   * @param array $options
   *   An array of options to apply.
   *
   * @return \Drupal\Core\Database\Query\SelectInterface|null
   *   Returns the entity select query.
   */
  public function query(AccountInterface $account, array $options = []): ?SelectInterface;

  /**
   * Builds extra entity queries.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The users account.
   * @param array $options
   *   An array of options to apply.
   *
   * @return \Drupal\Core\Database\Query\SelectInterface[]
   *   Returns an array of extra queries.
   */
  public function queryExtra(AccountInterface $account, array $options = []): array;

  /**
   * Get entity item text.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param array $data
   *   An array of additional token replacement data.
   *
   * @return \Drupal\Component\Render\MarkupInterface
   *   Returns the text with tokens replaced.
   */
  public function text(EntityInterface $entity, array $data = []): MarkupInterface;

  /**
   * Render item entity view mode.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return array
   *   Returns the entity build array.
   */
  public function renderEntity(EntityInterface $entity): array;

  /**
   * Get item filter facets.
   *
   * @param bool $combine
   *   Boolean indicating to combine facets into single value.
   *
   * @return array
   *   Returns the facets.
   */
  public function getFacets(bool $combine = TRUE): array;

  /**
   * Get entity display view modes.
   *
   * @param string $id
   *   The entity type identifier.
   *
   * @return array
   *   Returns the display view modes.
   */
  public function getViewModes(string $id): array;

  /**
   * Defines additional token replacements.
   *
   * Use the copy-needs-data property in conjunction with needs-data
   * to replace tokens for multiple entities of the same type.
   *
   * @return array
   *   Returns associative array of token types or tokens.
   */
  public function tokenInfo(): array;

  /**
   * Get available token types.
   *
   * @return array
   *   Returns the token types.
   */
  public function getTokenTypes(): array;

  /**
   * Get entity definition.
   *
   * @param string|null $id
   *   The entity type identifier.
   *
   * @return \Drupal\Core\Entity\EntityTypeInterface|null
   *   Returns the loaded entity definition.
   */
  public function getEntityDefinition(string $id = NULL): ?EntityTypeInterface;

  /**
   * Get item entity bundles.
   *
   * @return \Drupal\Core\Entity\EntityTypeInterface[]
   *   Returns an array of compatible entity bundles.
   */
  public function getEntityBundles(): array;

  /**
   * Load multiple entities.
   *
   * @param string $entityTypeId
   *   The entity type identifier.
   * @param array|null $ids
   *   The optional entity IDs. Defaults to all entities.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]|\Drupal\Core\Entity\EntityTypeInterface[]
   *   Returns an array of loaded entities.
   */
  public function loadMultiple(string $entityTypeId, array $ids = NULL): array;

}
