<?php

namespace Drupal\entity_timeline\Plugin;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Config\Entity\ConfigEntityTypeInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageDefault;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Render\Markup;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Utility\Token;
use Drupal\user\EntityOwnerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for timeline item type plugins.
 */
abstract class TimelineItemTypeBase extends PluginBase implements TimelineItemTypeInterface, ContainerFactoryPluginInterface {

  /**
   * The base entity type.
   *
   * @var string
   */
  protected string $entityType;

  /**
   * The base entity type timestamp field.
   *
   * @var string
   */
  protected string $timestampField = 'created';

  /**
   * The base entity type definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface
   */
  protected EntityTypeInterface $entityTypeDefinition;

  /**
   * The base bundle entity type definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface
   */
  protected EntityTypeInterface $entityTypeBundleDefinition;

  /**
   * The entity storage table.
   *
   * @var string|null
   */
  protected ?string $table;

  /**
   * The entity storage table owner column.
   *
   * @var string|null
   */
  protected ?string $column;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $connection;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The token.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected Token $token;

  /**
   * The default language.
   *
   * @var \Drupal\Core\Language\LanguageDefault
   */
  protected LanguageDefault $languageDefault;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected AccountProxyInterface $currentUser;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected EntityDisplayRepositoryInterface $entityDisplayRepository;

  /**
   * The base query.
   *
   * @var \Drupal\Core\Database\Query\SelectInterface
   */
  protected SelectInterface $query;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->connection = $container->get('database');
    $instance->moduleHandler = $container->get('module_handler');
    $instance->languageDefault = $container->get('language.default');
    $instance->token = $container->get('token');
    $instance->currentUser = $container->get('current_user');
    $instance->entityFieldManager = $container->get('entity_field.manager');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->entityDisplayRepository = $container->get('entity_display.repository');
    $instance->entityType = key($plugin_definition['context_definitions']);
    $instance->entityTypeDefinition = $instance->getEntityDefinition($instance->entityType);
    $instance->entityTypeBundleDefinition = $instance->getEntityDefinition($instance->entityTypeDefinition->getBundleEntityType());
    if (!($instance->table = $instance->entityTypeDefinition->getDataTable())) {
      $instance->table = $instance->entityTypeDefinition->getBaseTable();
    }
    if (!($instance->column = $instance->entityTypeDefinition->getKey('owner'))) {
      $instance->column = 'uid';
    }
    $instance->setConfiguration($instance->configuration);
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'item_id' => NULL,
      'enabled' => [],
      'bundles' => [],
      'facets' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $bundles = $this->getEntityBundles();
    $form['enabled'] = [
      '#type' => 'checkboxes',
      '#title' => $this->entityTypeBundleDefinition->getLabel(),
      '#options' => $this->getBundleOptions($bundles),
      '#required' => TRUE,
      '#default_value' => $this->configuration['enabled'],
    ];

    $form['bundles'] = ['#tree' => TRUE];
    $viewModes = $this->getViewModes($this->entityType);
    foreach ($bundles as $id => $bundle) {
      $form['bundles'][$id] = [
        '#type' => 'details',
        '#title' => $bundle instanceof ContentEntityTypeInterface ? $bundle->getLabel() : $bundle->label(),
        '#states' => [
          'visible' => [
            ":input[name='data[enabled][$id]']" => ['checked' => TRUE],
          ],
        ],
      ];
      $form['bundles'][$id]['text'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Text'),
        '#description' => $this->t('Define the text message. Token replacements are supported.'),
        '#default_value' => $this->configuration['bundles'][$id]['text'] ?? NULL,
        '#states' => ['required' => $form['bundles'][$id]['#states']['visible']],
      ];
      $form['bundles'][$id]['pronoun'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Set pronoun text'),
        '#default_value' => $this->configuration['bundles'][$id]['pronoun'] ?? NULL,
      ];
      $form['bundles'][$id]['pronoun_text'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Pronoun text'),
        '#description' => $this->t('Define the text message with a possessive pronoun. Token replacements are supported.'),
        '#default_value' => $this->configuration['bundles'][$id]['pronoun_text'] ?? NULL,
        '#states' => [
          'visible' => [
            ":input[name='data[bundles][$id][pronoun]']" => ['checked' => TRUE],
          ],
        ],
      ];
      $form['bundles'][$id]['pronoun_text']['#states']['required'] = $form['bundles'][$id]['pronoun_text']['#states']['visible'];
      $form['bundles'][$id]['view_mode'] = [
        '#type' => 'select',
        '#title' => $this->t('View mode'),
        '#description' => $this->t('Show entity view mode with the text message.'),
        '#options' => $viewModes,
        '#default_value' => $this->configuration['bundles'][$id]['view_mode'] ?? NULL,
      ];
      $form['bundles'][$id]['facet'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Facet'),
        '#description' => $this->t('Show as available facet option.'),
        '#default_value' => $this->configuration['bundles'][$id]['facet'] ?? TRUE,
      ];
      $form['bundles'][$id]['facet_label'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Facet label'),
        '#description' => $this->t('Override the default facet label.'),
        '#default_value' => $this->configuration['bundles'][$id]['facet_label'] ?? NULL,
        '#states' => [
          'visible' => [
            ":input[name='data[bundles][$id][facet]']" => ['checked' => TRUE],
          ],
        ],
      ];
      $form['bundles'][$id]['access'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Check access'),
        '#description' => $this->t('Validate the user has view access.'),
        '#default_value' => $this->configuration['bundles'][$id]['access'] ?? TRUE,
      ];
    }

    $form['facets'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Facets'),
      '#tree' => TRUE,
    ];
    $form['facets']['combine'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Combine facets'),
      '#description' => $this->t('Use single facet for all available options.'),
      '#default_value' => $this->configuration['facets']['combine'] ?? FALSE,
    ];
    $form['facets']['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Combined facets label'),
      '#default_value' => $this->configuration['facets']['label'] ?? NULL,
      '#states' => [
        'required' => [
          ':input[name="data[facets][combine]"]' => ['checked' => TRUE],
        ],
        'visible' => [
          ':input[name="data[facets][combine]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['token_help'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => $this->getTokenTypes(),
      '#weight' => 100,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {

  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {

  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration(): array {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration): void {
    $this->configuration = $configuration + $this->defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function getBundleOptions(array $bundles): array {
    $options = [];
    foreach ($bundles as $bundle) {
      if ($bundle instanceof ContentEntityTypeInterface) {
        $options[$bundle->id()] = $bundle->getLabel();
      }
      else {
        $options[$bundle->id()] = $bundle->label();
      }
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function getBundleConfiguration(string $bundle): array {
    return ($this->configuration['bundles'][$bundle] ?? []) + [
      'access' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getTable(): ?string {
    return $this->table;
  }

  /**
   * {@inheritdoc}
   */
  public function getColumn(): ?string {
    return $this->column;
  }

  /**
   * {@inheritdoc}
   */
  public function query(AccountInterface $account, array $options = []): ?SelectInterface {
    $options += [
      'type' => NULL,
      'facets' => NULL,
    ];
    $id = $this->entityTypeDefinition->getKey('id');
    $bundle = $this->entityTypeDefinition->getKey('bundle');

    $this->query = $this->connection->select($this->table);
    $this->query->addTag("{$this->entityTypeDefinition->id()}_access");

    // Normalize field expressions and aggregation. Each query must match
    // match the unionized columns defined in TimelineBuilder::query().
    $this->query->addExpression("'$this->pluginId'", 'item_type');
    $this->query->addExpression("'{$this->configuration['item_id']}'", 'item_id');
    $this->query->addExpression("'{$this->entityTypeDefinition->id()}'", 'entity_type');
    $this->query->addExpression("$this->table.$id", 'entity_id');
    $this->query->groupBy("$this->table.$id");
    if ($bundle) {
      $this->query->addExpression("$this->table.$bundle", 'bundle');
      $this->query->condition("$this->table.$bundle", array_keys(array_filter($this->configuration['enabled'])), 'IN');
      $this->query->groupBy('bundle');

      if ($options['facets'] && is_array($options['facets'])) {
        // Apply condition for active facets.
        $this->query->condition("$this->table.$bundle", $options['facets'], 'IN');
      }
    }
    else {
      $this->query->addExpression("NULL", 'bundle');
    }
    if ($langcode = $this->entityTypeDefinition->getKey('langcode')) {
      $this->query->addExpression("$this->table.$langcode", 'langcode');
      if ($defaultLangcode = $this->entityTypeDefinition->getKey('default_langcode')) {
        // Limit to the default language.
        $this->query->condition("$this->table.$defaultLangcode", 1);
      }
      $this->query->groupBy('langcode');
    }
    else {
      $this->query->addExpression("'{$this->languageDefault->get()->getId()}'", 'langcode');
    }
    $this->query->addExpression($this->timestampField ? "$this->table.$this->timestampField" : "NULL", 'timestamp');

    return $this->query;
  }

  /**
   * {@inheritdoc}
   */
  public function queryExtra(AccountInterface $account, array $options = []): array {
    $queries = [];

    // Prepare user context conditions. Depends on the 'user_follow' flag
    // to include results of followed users. Only the current user may
    // view their own followed results.
    $extraQuery = clone $this->query;
    $alias = $extraQuery->innerJoin(
      'flagging',
      'flagging_follow',
      "flagging_follow.uid = :flag_uid AND flagging_follow.flag_id = :flag_id",
      [
        ':flag_uid' => $account->id(),
        ':flag_id' => 'follow_user',
      ]
    );
    $extraQuery->where("$this->table.$this->column = $alias.entity_id");
    $queries[] = $extraQuery;

    return $queries;
  }

  /**
   * {@inheritdoc}
   */
  public function text(EntityInterface $entity, array $data = []): MarkupInterface {
    $config = $this->getBundleConfiguration($entity->bundle());
    $text = $config['text'] ?? NULL;
    $data[$entity->getEntityTypeId()] = $entity;
    if ($entity instanceof EntityOwnerInterface) {
      $data['user'] = $entity->getOwner();
      if ($config['pronoun'] && $config['pronoun_text'] && $entity->getOwnerId() === $this->currentUser->id()) {
        $text = $config['pronoun_text'];
      }
    }
    return Markup::create(Xss::filterAdmin($this->token->replace($text, $data)));
  }

  /**
   * {@inheritdoc}
   */
  public function renderEntity(EntityInterface $entity): array {
    $config = $this->getBundleConfiguration($entity->bundle());
    if (isset($config['view_mode']) && $config['view_mode']) {
      // Invoke hook_timeline_ENTITY_TYPE_view().
      $build = $this->moduleHandler->invokeAll(
        "entity_timeline_{$entity->getEntityTypeId()}_view",
        [$entity, $config]
      );
      if ($build) {
        return $build;
      }
      return $this->entityTypeManager->getViewBuilder($entity->getEntityTypeId())->view($entity, $config['view_mode']);
    }
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getFacets(bool $combine = TRUE): array {
    $values = [];
    if ($combine && $this->configuration['facets']['combine']) {
      $values['*'] = $this->configuration['facets']['label'];
    }
    else {
      $bundles = $this->getEntityBundles();
      foreach (array_keys(array_filter($this->configuration['enabled'])) as $id) {
        if (isset($bundles[$id]) && $this->configuration['bundles'][$id]['facet']) {
          $bundle = $bundles[$id];
          $values[$id] = $this->configuration['bundles'][$id]['facet_label'] ?: ($bundle instanceof ContentEntityTypeInterface ? $bundle->getLabel() : $bundle->label());
        }
      }
    }
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function getViewModes(string $id): array {
    $options = ['' => $this->t('- None -')];
    foreach ($this->entityDisplayRepository->getViewModes($id) as $modeId => $mode) {
      $options[$modeId] = $mode['label'];
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function tokenInfo(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getTokenTypes(): array {
    return ['user', $this->entityType];
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityDefinition(string $id = NULL): ?EntityTypeInterface {
    if (!$id) {
      return $this->entityTypeDefinition;
    }
    return $this->entityTypeManager->getDefinition($id);
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityBundles(): array {
    if ($this->entityTypeBundleDefinition instanceof ConfigEntityTypeInterface) {
      return $this->loadMultiple($this->entityTypeBundleDefinition->id());
    }
    else {
      return [$this->entityTypeBundleDefinition->id() => $this->entityTypeBundleDefinition];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function loadMultiple(string $entityTypeId, array $ids = NULL): array {
    return $this->entityTypeManager->getStorage($entityTypeId)->loadMultiple($ids);
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies(): array {
    return [];
  }

}
