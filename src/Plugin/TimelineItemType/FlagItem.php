<?php

namespace Drupal\entity_timeline\Plugin\TimelineItemType;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\entity_timeline\Plugin\TimelineItemTypeBase;
use Drupal\flag\FlagServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Override flag entity bundle timeline item type plugin.
 *
 * @TimelineItemType(
 *   id = "entity_bundle:flagging",
 * )
 */
class FlagItem extends TimelineItemTypeBase {

  /**
   * The flag manager.
   *
   * @var \Drupal\flag\FlagServiceInterface
   */
  protected FlagServiceInterface $flagManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->flagManager = $container->get('flag');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $bundles = $this->loadMultiple($this->entityTypeBundleDefinition->id());
    foreach ($bundles as $id => $bundle) {
      /** @var \Drupal\flag\FlagInterface $bundle */
      $targetEntityType = $this->getEntityDefinition($bundle->getFlaggableEntityTypeId());
      $form['bundles'][$id]['view_mode']['#options'] = $this->getViewModes($targetEntityType->id());
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function query(AccountInterface $account, array $options = []): ?SelectInterface {
    $query = parent::query($account, $options);
    if ($this->currentUser->id() === $account->id() && $options['type'] === 'follow') {
      $query->condition("flagging.$this->column", $account->id(), '!=');
    }
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function text(EntityInterface $entity, array $data = []): MarkupInterface {
    /** @var \Drupal\flag\Entity\Flagging $entity */
    $type = $entity->getFlaggableType();
    if (in_array($type, $this->getFlaggableEntityTypeIds(), TRUE)) {
      $data["flag_$type"] = $entity->getFlaggable();
    }
    return parent::text($entity, $data);
  }

  /**
   * {@inheritdoc}
   */
  public function tokenInfo(): array {
    $info = [];
    foreach ($this->getFlaggableEntityTypeIds() as $id) {
      if ($definition = $this->getEntityDefinition($id)) {
        $label = mb_strtolower($definition->getLabel());
        $info['types']["flag-$id"] = [
          'name' => $this->t('Flagged @label', [
            '@label' => $label,
          ]),
          'description' => $this->t('Tokens related to the flagged @label.', [
            '@label' => $label,
          ]),
          'needs-data' => "flag_$id",
          'copy-needs-data' => $id,
        ];
        $info['tokens']["flag-$id"] = [];
      }
    }
    return $info;
  }

  /**
   * {@inheritdoc}
   */
  public function getTokenTypes(): array {
    $types = [];
    foreach ($this->getFlaggableEntityTypeIds() as $id) {
      $types[] = "flag-$id";
    }
    return $types + parent::getTokenTypes();
  }

  /**
   * Get flaggable entity types.
   *
   * @return array
   *   Returns an array of entity type identifiers.
   */
  public function getFlaggableEntityTypeIds(): array {
    $ids = [];
    foreach ($this->flagManager->getAllFlags() as $flag) {
      $key = $flag->getFlaggableEntityTypeId();
      $ids[$key] = $key;
    }
    return $ids;
  }

}
