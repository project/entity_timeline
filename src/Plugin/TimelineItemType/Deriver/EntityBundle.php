<?php

namespace Drupal\entity_timeline\Plugin\TimelineItemType\Deriver;

use Drupal\Core\Entity\Plugin\Condition\Deriver\EntityBundle as SourceEntityBundle;
use Drupal\Core\Plugin\Context\EntityContextDefinition;

/**
 * Deriver that creates a timeline item for each entity type with bundles.
 */
class EntityBundle extends SourceEntityBundle {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition): array {
    $schema = \Drupal::database()->schema();
    foreach ($this->entityTypeManager->getDefinitions() as $id => $type) {
      $definition = $this->entityTypeManager->getDefinition($id);
      if (!$definition->getBaseTable() && !$definition->getDataTable()) {
        continue;
      }

      // Table must have an owner column to query.
      if (!($table = $definition->getDataTable())) {
        $table = $definition->getBaseTable();
      }
      if (!($column = $definition->getKey('owner'))) {
        $column = $schema->fieldExists($table, 'uid') ? 'uid' : FALSE;
      }
      if (!$table || !$column) {
        continue;
      }

      $this->derivatives[$id] = $base_plugin_definition;
      $this->derivatives[$id]['label'] = $type->getLabel();
      $this->derivatives[$id]['provider'] = $type->getProvider();
      $this->derivatives[$id]['context_definitions'] = [
        $id => EntityContextDefinition::fromEntityType($type),
      ];
    }
    return $this->derivatives;
  }

}
