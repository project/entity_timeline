<?php

namespace Drupal\entity_timeline\Plugin\TimelineItemType;

use Drupal\entity_timeline\Plugin\TimelineItemTypeBase;

/**
 * Provides entity bundle timeline item type plugin.
 *
 * @TimelineItemType(
 *   id = "entity_bundle",
 *   deriver = "\Drupal\entity_timeline\Plugin\TimelineItemType\Deriver\EntityBundle",
 * )
 */
class EntityBundle extends TimelineItemTypeBase {

}
