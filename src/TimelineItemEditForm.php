<?php

namespace Drupal\entity_timeline;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;

/**
 * The timeline item entity edit form.
 *
 * @property \Drupal\entity_timeline\TimelineItemInterface $entity
 */
class TimelineItemEditForm extends TimelineItemForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);
    $form['plugin']['#disabled'] = TRUE;

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->entity->status(),
      '#weight' => 50,
    ];
    $form['actions']['#weight'] = 100;

    // Build the plugin configuration.
    $form['data'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Configuration'),
      '#tree' => TRUE,
    ];
    $subformState = SubformState::createForSubform($form['data'], $form, $form_state);
    $form['data'] = $this->entity->getPlugin()->buildConfigurationForm($form['data'], $subformState);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $subformState = SubformState::createForSubform($form['data'], $form, $form_state);
    $this->entity->getPlugin()->validateConfigurationForm($form['data'], $subformState);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $subformState = SubformState::createForSubform($form['data'], $form, $form_state);
    $this->entity->getPlugin()->submitConfigurationForm($form['data'], $subformState);
    $this->entity->set('configuration', $form_state->getValue('data'));
  }

}
