<?php

namespace Drupal\entity_timeline;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\PagerSelectExtender;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Http\RequestStack;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Pager\PagerManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\entity_timeline\Entity\TimelineItem;

/**
 * Provides timeline builder.
 */
class TimelineBuilder {

  use StringTranslationTrait;

  /**
   * The request stack.
   *
   * @var \Drupal\Core\Http\RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $connection;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected AccountProxyInterface $currentUser;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * The users account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected AccountInterface $account;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The pager manager.
   *
   * @var \Drupal\Core\Pager\PagerManagerInterface
   */
  protected PagerManagerInterface $pagerManager;

  /**
   * The timeline items.
   *
   * @var \Drupal\entity_timeline\TimelineItemInterface[]
   */
  protected array $items;

  /**
   * The plugin map records.
   *
   * @var array
   */
  protected array $map = [];

  /**
   * The loaded entities.
   *
   * @var \Drupal\Core\Entity\EntityInterface[]
   */
  protected array $entities = [];

  /**
   * The loaded entity type definitions.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface[]
   */
  protected array $entityDefinitions = [];

  /**
   * TimelineBuilder constructor.
   *
   * @param \Drupal\Core\Http\RequestStack $requestStack
   *   The request stack.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Pager\PagerManagerInterface $pagerManager
   *   The pager manager.
   */
  public function __construct(RequestStack $requestStack, Connection $connection, ModuleHandlerInterface $moduleHandler, AccountProxyInterface $currentUser, LanguageManagerInterface $languageManager, EntityTypeManagerInterface $entityTypeManager, PagerManagerInterface $pagerManager) {
    $this->requestStack = $requestStack;
    $this->connection = $connection;
    $this->moduleHandler = $moduleHandler;
    $this->currentUser = $currentUser;
    $this->languageManager = $languageManager;
    $this->entityTypeManager = $entityTypeManager;
    $this->pagerManager = $pagerManager;
    $this->setAccount($currentUser);

    // Get enabled timeline items.
    $this->items = array_filter(TimelineItem::loadMultiple(), function ($entity) {
      return $entity->status();
    });
    uasort($this->items, function ($a, $b) {
      return $a->get('weight') <=> $b->get('weight');
    });
  }

  /**
   * Build the timeline.
   *
   * @return array
   *   Returns a build array.
   */
  public function build(): array {
    $build = [
      '#attached' => [
        'library' => ['entity_timeline/items'],
      ],
    ];
    $links = $items = [];
    $build['results'] = [
      '#theme' => 'entity_timeline',
      '#links' => &$links,
      '#content' => &$items,
    ];

    $cache = new CacheableMetadata();
    if ($results = $this->query()) {
      $build['pager'] = [
        '#type' => 'pager',
        '#element' => $this->pagerManager->getMaxPagerElementId(),
      ];
      foreach ($results as $record) {
        $plugin = $this->items[$record->item_id]->getPlugin();

        // Check access and allow modules to alter access result.
        $access = !$plugin->getBundleConfiguration($record->entity->bundle())['access'] || $record->entity->access('view');
        $this->moduleHandler->alter('entity_timeline_item_access', $access, $record->entity, $plugin);
        if (!$access) {
          continue;
        }

        // Prefer translation matching the current language.
        $langcode = $this->languageManager->getCurrentLanguage()->getId();
        if ($record->entity instanceof ContentEntityInterface && $record->entity->hasTranslation($langcode)) {
          $record->entity = $record->entity->getTranslation($langcode);
        }

        $items[] = [
          '#theme' => 'entity_timeline_item',
          '#item' => $this->items[$record->item_id],
          '#record' => $record,
          '#text' => $plugin->text($record->entity),
          '#rendered_entity' => $plugin->renderEntity($record->entity),
        ];

        // Merge and use the entities cache settings. Similar to views,
        // each entity result adds to the overall cache strategy. Cache
        // is further varied by the current user context.
        $cache->addCacheableDependency($record->entity)->addCacheTags($record->entity_definition->getListCacheTags());
      }
    }

    $cache->setCacheContexts(['route', 'url.query_args', 'user']);
    $cache->addCacheableDependency($this->account);
    foreach ($this->items as $item) {
      $cache->addCacheTags(['config:timeline_item_list']);
      $cache->addCacheableDependency($item)->addCacheTags($item->getPlugin()->getEntityDefinition()->getListCacheTags());
    }
    $cache->applyTo($build);

    return $build;
  }

  /**
   * Build and execute the query.
   *
   * @param int $limit
   *   The pagers limit.
   *
   * @return array
   *   Returns an array of results.
   */
  public function query(int $limit = 20): array {
    if (!$this->items) {
      return [];
    }
    $facets = $this->getActiveFacets();

    // Unionize the plugin queries.
    $queries = [];
    foreach ($this->items as $item) {
      if (!$facets || isset($facets[$item->id()])) {
        $options = [
          'type' => $this->getActiveType(),
          'facets' => $facets[$item->id()] ?? [],
        ];
        $plugin = $item->getPlugin();
        if ($query = $plugin->query($this->account, $options)) {
          if ($options['type'] !== 'follow') {
            $queries[] = (clone $query)->condition("{$plugin->getTable()}.{$plugin->getColumn()}", $this->account->id());
          }
          if ($this->currentUser->id() === $this->account->id() && (!$options['type'] || $options['type'] === 'follow')) {
            $queries = array_merge($queries, $plugin->queryExtra($this->account, $options));
          }
        }
      }
    }
    if (!$queries) {
      return [];
    }
    /** @var \Drupal\Core\Database\Query\Select $union */
    $union = array_shift($queries);
    foreach ($queries as $query) {
      $union->union($query);
    }

    /** @var \Drupal\Core\Database\Query\Select $query */
    $query = $this->connection->select($union)
      ->fields(NULL, [
        'item_type',
        'item_id',
        'entity_type',
        'entity_id',
        'bundle',
        'langcode',
        'timestamp',
      ])
      ->orderBy('timestamp', 'DESC')
      ->extend(PagerSelectExtender::class)
      ->limit($limit);

    // Execute query and prepare results. Attaches the loaded entity object
    // and definition for additional processing.
    $results = $query->execute()->fetchAll();
    foreach ($results as $record) {
      $this->map[$record->item_id][$record->entity_type][$record->entity_id] = $record->entity_id;
    }
    $this->loadMultiple($this->map);
    array_walk($results, function ($record, $key) use (&$results): void {
      if (isset($this->entities[$record->entity_type][$record->entity_id])) {
        $record->entity = $this->entities[$record->entity_type][$record->entity_id];
        $record->entity_definition = $this->entityDefinitions[$record->entity_type];
      }
      else {
        unset($results[$key]);
      }
    });
    return $results;
  }

  /**
   * Get timeline items.
   *
   * @return \Drupal\entity_timeline\TimelineItemInterface[]
   *   Returns an array of enabled timeline items.
   */
  public function getItems(): array {
    return $this->items;
  }

  /**
   * Get active type filter.
   *
   * @return string
   *   Returns the active type.
   */
  public function getActiveType(): ?string {
    $type = $this->requestStack->getCurrentRequest()->query->get('type');
    if (in_array($type, ['user', 'follow'], TRUE)) {
      return $type;
    }
    return NULL;
  }

  /**
   * Get active item filter facets.
   *
   * @return array
   *   Returns the active facets.
   */
  public function getActiveFacets(): array {
    $values = [];
    if ($rawValues = $this->requestStack->getCurrentRequest()->query->get('facets')) {
      foreach ($rawValues as $value) {
        [$id, $key] = explode('-', $value);
        $plugin = $this->items[$id]->getPlugin();
        if (isset($this->items[$id], $this->items[$id]->getPlugin()->getFacets()[$key])) {
          if ($key === '*') {
            // On combined facets get every enabled facet.
            foreach ($plugin->getFacets(FALSE) as $key => $label) {
              $values[$id][$key] = $key;
            }
          }
          else {
            $values[$id][$key] = $key;
          }
        }
      }
    }
    return $values;
  }

  /**
   * Load multiple entities and definitions.
   *
   * @param array $map
   *   An array of plugin map records.
   */
  public function loadMultiple(array $map): void {
    foreach ($map as $key => $items) {
      $plugin = $this->items[$key]->getPlugin();
      if (!isset($this->entities[key($items)])) {
        $this->entities[key($items)] = [];
      }
      $this->entities[key($items)] += $plugin->loadMultiple(key($items), current($items));
      $this->entityDefinitions[key($items)] = $plugin->getEntityDefinition(key($items));
    }
  }

  /**
   * Set users account.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user.
   */
  public function setAccount(AccountInterface $account): TimelineBuilder {
    if ($account instanceof AccountProxyInterface) {
      $account = $this->entityTypeManager->getStorage('user')->load($account->id());
    }
    $this->account = $account;
    return $this;
  }

}
