<?php

namespace Drupal\entity_timeline\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\entity_timeline\TimelineBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the timeline facets form.
 */
class TimelineFacetsForm extends FormBase {

  /**
   * The timeline builder.
   *
   * @var \Drupal\entity_timeline\TimelineBuilder
   */
  protected TimelineBuilder $timelineBuilder;

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'entity_timeline_facets_form';
  }

  /**
   * BaseCollectionForm constructor.
   *
   * @param \Drupal\entity_timeline\TimelineBuilder $timelineBuilder
   *   The timeline builder.
   */
  public function __construct(TimelineBuilder $timelineBuilder) {
    $this->timelineBuilder = $timelineBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): TimelineFacetsForm {
    return new static(
      $container->get('entity_timeline.builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $values = $this->getRequest()->query->get('facets');
    $form['facets'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Filters'),
      '#options' => [],
      '#default_value' => is_array($values) ? $values : [],
    ];
    foreach ($this->timelineBuilder->getItems() as $item) {
      foreach ($item->getPlugin()->getFacets() as $key => $label) {
        $form['facets']['#options']["{$item->id()}-$key"] = $label;
      }
    }
    if (!$values) {
      $form['facets']['#default_value'] = array_combine(
        array_keys($form['facets']['#options']),
        array_keys($form['facets']['#options'])
      );
    }
    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Apply'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $url = Url::fromUserInput($this->getRequest()->getRequestUri(), [
      'query' => ['facets' => array_keys(array_filter($form_state->getValue('facets')))],
    ]);
    $query = $url->getOption('query');
    unset($query['page']);
    $url->setOption('query', $query);
    $form_state->setRedirectUrl($url);
  }

}
