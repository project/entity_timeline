<?php

namespace Drupal\entity_timeline\Annotation;

use Drupal\Core\Condition\Annotation\Condition;

/**
 * Defines the timeline item type plugin annotation.
 *
 * @see \Drupal\entity_timeline\Plugin\TimelineItemTypeInterface
 * @see \Drupal\entity_timeline\Plugin\TimelineItemTypeManager
 * @see plugin_api
 *
 * @Annotation
 */
class TimelineItemType extends Condition {

}
