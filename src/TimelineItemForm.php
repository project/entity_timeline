<?php

namespace Drupal\entity_timeline;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entity_timeline\Entity\TimelineItem;
use Drupal\entity_timeline\Plugin\TimelineItemTypeManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The timeline item entity form.
 *
 * @property \Drupal\entity_timeline\TimelineItemInterface $entity
 */
class TimelineItemForm extends BundleEntityFormBase {

  /**
   * The timeline item type manager.
   *
   * @var \Drupal\entity_timeline\Plugin\TimelineItemTypeManager
   */
  protected TimelineItemTypeManager $timelineItemTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): TimelineItemForm {
    $instance = new static();
    $instance->timelineItemTypeManager = $container->get('plugin.manager.entity_timeline.item_type');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    $timelineItem = $this->entity;
    if ($this->operation === 'edit') {
      $form['#title'] = $this->t('Edit %label timeline item', ['%label' => $timelineItem->label()]);
    }

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#description' => $this->t('Label for the timeline item.'),
      '#maxlength' => 255,
      '#required' => TRUE,
      '#default_value' => $timelineItem->label(),
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#machine_name' => [
        'exists' => [TimelineItem::class, 'load'],
      ],
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#default_value' => $timelineItem->id(),
    ];
    $form['plugin'] = [
      '#type' => 'select',
      '#title' => $this->t('Type'),
      '#description' => $this->t('Type of data for the timeline item.'),
      '#options' => [],
      '#required' => TRUE,
      '#default_value' => $timelineItem->getPluginId(),
    ];
    foreach ($this->timelineItemTypeManager->getDefinitions() as $id => $definition) {
      $form['plugin']['#options'][$id] = $definition['label'];
    }
    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];
    return $this->protectBundleIdElement($form);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->messenger()->addStatus($this->t('Your settings have been saved.'));
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    parent::save($form, $form_state);
    $form_state->setRedirectUrl($this->entity->toUrl('edit-form'));
  }

}
