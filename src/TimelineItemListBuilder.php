<?php

namespace Drupal\entity_timeline;

use Drupal\Core\Config\Entity\DraggableListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * List builder for timeline item entity.
 *
 * @see \Drupal\entity_timeline\Entity\TimelineItem
 */
class TimelineItemListBuilder extends DraggableListBuilder {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'entity_timeline_item_list';
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['label'] = $this->t('Label');
    $header['plugin'] = $this->t('Type');
    $header['status'] = $this->t('Status');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\entity_timeline\TimelineItemInterface $entity */
    $row['label'] = $entity->label();
    $row['plugin'] = [
      '#markup' => $entity->getPlugin()->getPluginDefinition()['label'],
    ];
    $row['status'] = [
      '#markup' => $entity->status() ? $this->t('Enabled') : $this->t('Disabled'),
    ];
    return $row + parent::buildRow($entity);
  }

}
