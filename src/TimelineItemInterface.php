<?php

namespace Drupal\entity_timeline;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\entity_timeline\Plugin\TimelineItemTypeInterface;
use Drupal\entity_timeline\Plugin\TimelineItemTypeManager;

/**
 * Provides an interface defining a timeline item entity.
 */
interface TimelineItemInterface extends ConfigEntityInterface {

  /**
   * Get the plugin manager.
   *
   * @return \Drupal\entity_timeline\Plugin\TimelineItemTypeManager
   *   Returns the plugin manager.
   */
  public function getPluginManager(): TimelineItemTypeManager;

  /**
   * Get the plugin identifier.
   *
   * @return string|null
   *   Returns the plugin identifier.
   */
  public function getPluginId(): ?string;

  /**
   * Get the plugin instance.
   *
   * @return \Drupal\entity_timeline\Plugin\TimelineItemTypeInterface|null
   *   Returns the plugin instance.
   */
  public function getPlugin(): ?TimelineItemTypeInterface;

}
