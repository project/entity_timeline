<?php

namespace Drupal\entity_timeline\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\entity_timeline\Plugin\TimelineItemTypeInterface;
use Drupal\entity_timeline\Plugin\TimelineItemTypeManager;
use Drupal\entity_timeline\TimelineItemInterface;

/**
 * Defines the timeline item configuration entity.
 *
 * @ConfigEntityType(
 *   id = "timeline_item",
 *   label = @Translation("Timeline item"),
 *   label_collection = @Translation("Timeline items"),
 *   label_singular = @Translation("timeline item"),
 *   label_plural = @Translation("timeline items"),
 *   label_count = @PluralTranslation(
 *     singular = "@count timeline item",
 *     plural = "@count timeline items",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\entity_timeline\TimelineItemListBuilder",
 *     "form" = {
 *       "default" = "Drupal\entity_timeline\TimelineItemForm",
 *       "add" = "Drupal\entity_timeline\TimelineItemForm",
 *       "edit" = "Drupal\entity_timeline\TimelineItemEditForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer timeline items",
 *   config_prefix = "timeline_item",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "weight" = "weight",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/timeline/timeline/add",
 *     "edit-form" = "/admin/structure/timeline/timeline/manage/{timeline_item}",
 *     "delete-form" = "/admin/structure/timeline/timeline/manage/{timeline_item}/delete",
 *     "collection" = "/admin/structure/timeline/timeline"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "plugin",
 *     "configuration",
 *     "weight",
 *     "status",
 *   }
 * )
 */
class TimelineItem extends ConfigEntityBase implements TimelineItemInterface {

  /**
   * The timeline item ID.
   *
   * @var string
   */
  protected string $id;

  /**
   * The timeline item label.
   *
   * @var string
   */
  protected string $label;

  /**
   * The timeline item plugin.
   *
   * @var string
   */
  protected string $plugin = '';

  /**
   * The timeline item weight.
   *
   * @var int
   */
  protected int $weight = 0;

  /**
   * The timeline item status.
   *
   * @var bool
   */
  protected $status = FALSE;

  /**
   * The timeline item configuration.
   *
   * @var array
   */
  protected array $configuration = [];

  /**
   * {@inheritdoc}
   */
  public function getPluginManager(): TimelineItemTypeManager {
    return \Drupal::service('plugin.manager.entity_timeline.item_type');
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginId(): ?string {
    return $this->plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function getPlugin(): ?TimelineItemTypeInterface {
    $this->configuration['item_id'] = $this->id;
    if ($instance = $this->getPluginManager()->createInstance($this->getPluginId(), $this->configuration)) {
      /** @var \Drupal\entity_timeline\Plugin\TimelineItemTypeInterface $instance */
      return $instance;
    }
    return NULL;
  }

}
