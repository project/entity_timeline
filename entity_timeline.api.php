<?php

/**
 * @file
 * Hooks provided by the Entity Timeline module.
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\entity_timeline\Plugin\TimelineItemTypeInterface;
use Drupal\flag\FlaggingInterface;

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter entity timeline item access.
 *
 * @param bool $access
 *   The access result.
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   The entity.
 * @param \Drupal\entity_timeline\Plugin\TimelineItemTypeInterface $plugin
 *   The timeline item plugin.
 */
function hook_entity_timeline_item_access_alter(bool &$access, EntityInterface $entity, TimelineItemTypeInterface $plugin): void {
  if ($entity instanceof FlaggingInterface) {
    $access = $entity->getFlaggable()->access('view');
  }
}

/**
 * Override entity timeline item rendered entity.
 *
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   The entity.
 * @param array $config
 *   The timeline item configuration.
 *
 * @return array
 *   Return build array to replace default render.
 */
function hook_entity_timeline_ENTITY_TYPE_view(EntityInterface $entity, array $config): array {
  return [
    '#markup' => t('Render this instead'),
  ];
}

/**
 * @} End of "addtogroup hooks".
 */
