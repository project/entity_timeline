# Entity Timeline

Display combined user activity from multiple entity types in timeline fashion. Plugin manager allows the inclusion
of additional content entity types. Timeline and facets are available to render as blocks.

## Usage

1. Download and install the `drupal/entity_timeline` module. Recommended install method is composer:
   ```
   composer require drupal/entity_timeline
   ```
2. Go to /admin/structure/timeline and create one or more timeline items.
3. Place "Timeline" block into the desired region. Or section when using layout builder.
4. (optional) Place "Timeline links" and "Timeline facets" blocks for timeline filters.

## Flag dependency

To provide the user follow functionality this module depends on the flag module. Upon installation the `follow_user`
flag is automatically created. Be sure to place the flag link in a user view mode or by other means.

> **NOTE:** Only the current user timeline includes activity from users they follow.

## Plugins

Derivatives of entity bundle plugins may be overridden to extend or change default behavior. Only bundled entity
types include derivatives out-of-box. For example, to override the node plugin create the
`src/Plugin/TimelineItemType/NodeItem.php` file with this code.

```php
<?php

namespace Drupal\example\Plugin\TimelineItemType;

use Drupal\entity_timeline\Plugin\TimelineItemTypeBase;

/**
 * Override node entity bundle timeline item type plugin.
 *
 * @TimelineItemType(
 *   id = "entity_bundle:node",
 * )
 */
class NodeItem extends TimelineItemTypeBase {

}
```
